package com.shahzad.rest;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

	public class AppConfiguration extends Configuration {
	    
	    private String template;
	    
	    private String defaultName = "Stranger";
	    
	    private String userName = "Afzal";

	    @JsonProperty
	    public String getTemplate() {
	        return template;
	    }

	    @JsonProperty
	    public void setTemplate(String template) {
	        this.template = template;
	    }

	    @JsonProperty
	    public String getDefaultName() {
	        return defaultName;
	    }

	    @JsonProperty
	    public void setDefaultName(String name) {
	        this.defaultName = name;
	    }
	    @JsonProperty
	    public String getUserName() {
	        return userName;
	    }

	    @JsonProperty
	    public void setUserName(String name) {
	        this.userName = name;
	    }
	   
	    @JsonProperty
	    private DataSourceFactory database = new DataSourceFactory();

	    public DataSourceFactory getDataSourceFactory() {
	    	this.database.setUrl("jdbc:postgresql://localhost:5432/mydb"); 
	    	this.database.setDriverClass("org.postgresql.Driver"); 
	    	this.database.setLogValidationErrors(true); 
	    	this.database.setUser("postgres");
	    	this.database.setPassword("master24");
	    	this.database.setValidationQuery("select version()"); 
	        return database;
	    }
	    
	}
