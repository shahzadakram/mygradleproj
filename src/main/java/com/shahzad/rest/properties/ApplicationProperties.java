package com.shahzad.rest.properties;

import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ApplicationProperties extends Properties {

    private static final Logger logger = Logger.getLogger(ApplicationProperties.class);

    private static final long serialVersionUID = -2241948961525135170L;

    private static ApplicationProperties instance = null;

    public static final String POOL_PROPERTY="pool.name";  

    protected ApplicationProperties() {

    }

    public static ApplicationProperties getInstance() 
    {
        if (instance == null) 
        {
            instance = new ApplicationProperties();
            // filling defaultProperties

            instance.defaults = new Properties();
            try 
            {
            	logger.info("instance.getClass().getClassLoader() = ;" + instance.getClass().getClassLoader() + ";");              
            	instance.defaults.load(instance.getClass().getClassLoader().getResourceAsStream("application.properties"));  
            	
            }//try
            catch (IOException e) 
            {
                logger.error("Unable read default properties ");
            }//catch
        }//if
        return instance;
    }

    public String get(String key) {
        return getProperty(key);
    }

    public String getPoolName() {
        return getProperty(POOL_PROPERTY);
    }

    
    public String getStringValue(String key) {
        return getProperty(key);
    }

    public int getIntegerValue(String key) {
        return Integer.parseInt(getProperty(key));
    }

    public long getLongValue(String key) {
        return Long.parseLong(getProperty(key));
    }

    public float getFloatValue(String key) {
        return Float.parseFloat(getProperty(key));
    }

    public double getDoubleValue(String key) {
        return Double.parseDouble(getProperty(key));
    }

    public boolean getBooleanValue(String key) {
        return Boolean.valueOf(getProperty(key)).booleanValue();
    }

    public Properties getSubProperties(String string) {
        String startString = string + ".";
        Properties result = new Properties();
        Iterator<Object> keys = this.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (key.startsWith(startString)) {
                result.put(key.substring(startString.length()), this.getProperty(key));
            }
        }
        return result;
    }
}
