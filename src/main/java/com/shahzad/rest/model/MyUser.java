package com.shahzad.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class MyUser {

    @NotNull
    @JsonProperty
    private Integer userId;  

    @NotNull
    @JsonProperty
    private String firstName;

    @NotNull
    @JsonProperty
    private String lastName;

   	@NotNull
    @JsonProperty
    private String userPassword;

   	@JsonProperty
    private String email;
   	
   	@JsonProperty
    private String phoneNumber;
   	
   	@JsonProperty
    private String dob;
   	
   	@JsonProperty
    private String gender;
   	
   	@JsonProperty
    private String address;
   	
   	@JsonProperty
    private String city;
   	
   	@JsonProperty
    private String state;
   	
   	@JsonProperty
    private String zip;
   	
   	@JsonProperty
    private String comments;
   	
   	
    public MyUser() {
        // Jackson deserialization
    }

    public MyUser(Integer userId, String firstName, String lastName, String userPassword, String email,
			String phoneNumber, String dob, String gender, String address, String city, String state, String zip,
			String comments) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userPassword = userPassword;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.dob = dob;
		this.gender = gender;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.comments = comments;
	}

	public Integer getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
		return lastName;
	}

	public String getUserPassword() {
		return userPassword;
	}
	
	public String getEmail() {
		return email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getDob() {
		return dob;
	}

	public String getGender() {
		return gender;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZip() {
		return zip;
	}

	public String getComments() {
		return comments;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MyUser)) return false;

        MyUser that = (MyUser) o;

        if (!getUserId().equals(that.getUserId())) return false;
        if (!getFirstName().equals(that.getFirstName())) return false;
        if (!getLastName().equals(that.getLastName())) return false;
        if (!getUserPassword().equals(that.getUserPassword())) return false;
        	//NEED TO WRITE CODE ....
        return true;
    }
}