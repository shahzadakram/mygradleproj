package com.shahzad.rest;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.jdbi.DBIFactory;


import com.shahzad.rest.resources.AppResource;
import com.shahzad.rest.resources.MyUserResource;
import com.shahzad.rest.health.TemplateHealthCheck;

import org.skife.jdbi.v2.DBI;

import com.shahzad.rest.MessageRestService;
import com.shahzad.rest.dao.MyUserDAO;

public class App extends Application<AppConfiguration> {
    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public String getName() {
        return "myApp";
    }

    @Override
    public void initialize(Bootstrap<AppConfiguration> bootstrap) {
        // nothing to do yet
    }

      
    @Override
    public void run(AppConfiguration configuration,
                    Environment environment) throws ClassNotFoundException {
    	
    	final DBIFactory factory = new DBIFactory();
    	final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "postgresql");
    	final MyUserDAO myUserDAO = jdbi.onDemand(MyUserDAO.class);
    	
    	environment.jersey().register(new MyUserResource(myUserDAO)); 
    	
        final AppResource resource = new AppResource(
            configuration.getTemplate(),
            configuration.getDefaultName(),
            configuration.getUserName()
        );
        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());
            environment.healthChecks().register("template", healthCheck
         );
            
        environment.jersey().register(resource);
        environment.jersey().register(new MessageRestService());
    }
}







