package com.shahzad.rest;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.shahzad.rest.model.Item;
import com.shahzad.rest.model.ItemList;

//http://localhost:8080/message/hello%20world
//http://localhost:8080/message/sum/3/4
//http://localhost:8080/message/item
	
@Path("/message")
public class MessageRestService {
	
	@GET
	@Path("/{param}")
	public Response printMessage(@PathParam("param") String msg) {

		String result = "Restful example : " + msg;

		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("/sum/{a}/{b}")
	@Produces("text/html")
	public String sum(@PathParam("a") int a, @PathParam("b") int b) {
		return String.valueOf(a + b);
	}

	
	@GET
	@Path("/item")
	@Produces({"application/xml"})
	public ItemList  getItem() {
	 
	ArrayList<Item> items = new ArrayList<Item> ();
	 
	Item item1 = new Item("computer",2500);	
	Item item2 = new Item("chair",100);
	Item item3 = new Item("table",200);
	
	items.add(item1);
	items.add(item2);
	items.add(item3);
		 
	 return new ItemList(items);
	 }	
}