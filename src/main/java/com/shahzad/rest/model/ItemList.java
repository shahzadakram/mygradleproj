package com.shahzad.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "listing")
public class ItemList extends Item {

	private List<Item> items;

	public ItemList() {
	}

	public ItemList(List<Item> items) {
		this.items = items;
	}

	@XmlElement(name = "items")
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
}
