# README #

### What is this repository for? ###

This is POC for Dropwizard + MicroServices(Rest) + Jetty + Postgres DB

### How do I get set up? ###

Pre-Req

1. JDK 1.8
2. Eclipse Oxygen 
	a. download Gradle plugin for Eclipse from Market place
	b. download Bootstrap plugin
	c. Github plugin
3. Gradle 3.4
4. Postgres 9.6
5. PgAdmin 1.6

### Contribution guidelines ###

Execute 'Gradle clean' task to clean the already build files

Execute 'Gradle oneJar' task to create a big fat jar

Execute 'Gradle run' task to run the project 

### Who do I talk to? ###

Shahzad Akram or Tone development team