package com.shahzad.rest.resources;

import com.shahzad.rest.api.Saying;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.util.concurrent.atomic.AtomicLong;
import java.util.Optional;

@Path("/myApp")
@Produces(MediaType.APPLICATION_JSON)
public class AppResource {
    private final String template;
    private final String defaultName;
    private final String userName;
    private final AtomicLong counter;
    

    public AppResource(String template, String defaultName, String userName) {
        this.template = template;
        this.defaultName = defaultName;
        this.userName = userName;
        this.counter = new AtomicLong();
    }
    
    @GET
    @Path("/sayHello")
    @Timed
    public Saying sayHello(@QueryParam("name") Optional<String> name) {
        final String value = String.format(template, name.orElse(defaultName));
        return new Saying(counter.incrementAndGet(), value);
    }
    
    @GET
    @Path("/getUser")
    public Saying getUser(@QueryParam("name") Optional<String> name) {
        final String value = String.format(template, name.orElse(userName));
        return new Saying(counter.incrementAndGet(), value+":mychange");
    }
}