package com.shahzad.rest.resources;

import com.shahzad.rest.model.MyUser;
import com.shahzad.rest.dao.MyUserDAO;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/myuser")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MyUserResource {

    MyUserDAO myUserDAO;

    public MyUserResource(MyUserDAO myUserDAO) {
        this.myUserDAO = myUserDAO;
    }

    @GET
    public List<MyUser> getAll(){
        return myUserDAO.getAll();
    }

    @GET
    @Path("/getUser/{user_id}")
    public MyUser get(@PathParam("user_id") Integer userId){
        return myUserDAO.findById(userId);
    }

    @POST
    public MyUser add(@Valid MyUser myUser) {
        myUserDAO.insert(myUser);

        return myUser;
    }

    @PUT
    @Path("/updateUser/{user_id}")
    public MyUser update(@PathParam("user_id") Integer id, @Valid MyUser myUser) {
    	MyUser updateMyUser = new MyUser(myUser.getUserId(), myUser.getFirstName(), myUser.getLastName(), myUser.getUserPassword(), myUser.getEmail(), myUser.getPhoneNumber(), myUser.getDob(), myUser.getGender(), myUser.getAddress(), myUser.getCity(), myUser.getState(), myUser.getZip(), myUser.getComments());       
    	myUserDAO.update(updateMyUser);

        return updateMyUser;
    }

    @DELETE
    @Path("/deleteUser/{user_id}")
    public void delete(@PathParam("user_id") Integer id) {
    	myUserDAO.deleteById(id);
    }
}