package com.shahzad.rest.mapper;

import com.shahzad.rest.model.MyUser;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MyUserMapper implements ResultSetMapper<MyUser>
{
    public MyUser map(int index, ResultSet resultSet, StatementContext statementContext) throws SQLException
    {
        return new MyUser(resultSet.getInt("USER_ID"), resultSet.getString("FIRST_NAME"), resultSet.getString("LAST_NAME"),resultSet.getString("USER_PASSWORD"), resultSet.getString("EMAIL"), resultSet.getString("PHONE_NUMBER"), resultSet.getString("DOB"), resultSet.getString("GENDER"), resultSet.getString("HOME_ADDRESS"), resultSet.getString("CITY"), resultSet.getString("STATE"), resultSet.getString("ZIP_CODE"), resultSet.getString("COMMENTS"));
    }
}