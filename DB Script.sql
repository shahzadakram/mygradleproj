-- DROP DATABASE mydb;

CREATE DATABASE mydb
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;

--USE MYDB;

-- DROP TABLE myuser;

CREATE TABLE myuser
(
  user_id integer NOT NULL,
  email character varying(100),
  first_name character varying(50) NOT NULL,
  last_name character varying(50) NOT NULL,
  user_password character varying(50) NOT NULL,
  phone_number character varying(12),
  home_address character varying(500),
  zip_code character varying(5),
  city character varying(50) NULL,
  state character varying(2) NULL,
  country character varying(50) NULL,
  status character varying(1) NULL,
  last_login date,
  created_on date NULL,
  created_by character varying(100),
  login_attempts integer,
  comments character varying(500),
  dob character varying(10),
  gender character varying(1),
  CONSTRAINT myuser_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE myuser
  OWNER TO postgres;
  