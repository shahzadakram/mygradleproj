package com.shahzad.rest.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.shahzad.rest.mapper.MyUserMapper;
import com.shahzad.rest.model.MyUser;


import java.util.List;

@RegisterMapper(MyUserMapper.class)
public interface MyUserDAO {
	  
	  @SqlQuery("select * from MYUSER")
	    List<MyUser> getAll();
	  
	  @SqlQuery("select * from MYUSER where USER_ID = :userId")
	    MyUser findById(@Bind ("userId") int userId);
	  
	  @SqlUpdate("delete from MYUSER where USER_ID = :userId")
	    int deleteById(@Bind("userId") int userId);

	  @SqlUpdate("update MYUSER set FIRST_NAME = :name where USER_ID = :userId")
	    int update(@BindBean MyUser myuser);

	  @SqlUpdate("insert into MYUSER (USER_ID, FIRST_NAME, LAST_NAME, USER_PASSWORD, EMAIL, PHONE_NUMBER, DOB, GENDER, HOME_ADDRESS, CITY, STATE, ZIP_CODE, COMMENTS) values (:userId, :firstName, :lastName, :userPassword, :email, :phoneNumber, :dob, :gender, :address, :city, :state, :zip, :comments)")
	    int insert(@BindBean MyUser myuser);
	}