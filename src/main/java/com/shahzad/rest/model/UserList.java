package com.shahzad.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "userList")
public class UserList extends User {

	private List<User> users;
	public UserList() {
	}

	public UserList(List<User> users) {
		this.users = users;
	}

	@XmlElement(name = "users")
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}